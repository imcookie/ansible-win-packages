# Роль для установки общего ПО

* роль не требует доступа к интернету, все необходимые установочные пакеты лежат в папке packages

* роль устанавливает notepad++ 7.8.1 (32 bit), dotnet-hosting-2.2.1, dotnet-sdk-2.2.103-win-x64, 7z, dotnet472, dotnet46, chrome

* роль частично имдепотетна, таски на установку dotnet46, dotnet472 не имдепотентны

* **роль при необходимости перезагрузит сервер**
* в папке scripts лежат скрипты для настройки Winrm ConfigureRemotingForAnsible.ps1, upgrade_to_ps3.ps1 (если старый powershell)

## Пример hosts файла для Windows

``` yaml
[win]
192.168.2.73
192.168.2.253

[win:vars]
ansible_user = ansible
ansible_password = changeme
ansible_connection = winrm
ansible_winrm_transport =  basic
ansible_winrm_server_cert_validation = ignore

 ```

## Пример playbook

``` yaml
---
- hosts:  win
  gather_facts: no
  roles:
  - ansible-win-packages
```
